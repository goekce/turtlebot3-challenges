# ROS development environment for robot challenges

Includes neovim and Codium

# Getting started

## Creating the container

Docker containers lose any file modifications when they are stopped, so create a volume where your projects and settings will be stored:
```
docker volume create home
```

Create a container called `ros` using the image from `registry.gitlab.com/goekce/turtlebot3-challenges`. `/dev/dri` is mounted to use the 3D GPU acceleration for graphical applications like the simulator `Gazebo`. 
```
xhost +local:
docker create \
        -it -e "DISPLAY=:0" \
        --mount type=bind,src=/tmp/.X11-unix,dst=/tmp/.X11-unix --device=/dev/dri:/dev/dri \
        --mount source=home,target=/home/u \
        --name ros --hostname ros \
        registry.gitlab.com/goekce/turtlebot3-challenges \
        /usr/bin/tmux
```
Check if the container is present:
```
docker container ls -a
```

## Starting the container

Start and get an interactive shell:
```
docker start -i ros
```
[tmux cheatsheet](https://tmuxcheatsheet.com)

You can also install additional software like `sudo apt install ...`. The image already contains

- `nvim`
- `codium` (VS Code that does not send any data to Microsoft)
  - start it using `codium --no-sandbox`, otherwise Codium will fail silently


## Pulling the latest updates to the image:

```
docker pull registry.gitlab.com/goekce/turtlebot3-challenges
```
