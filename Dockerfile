FROM ros:humble
ARG uid=1000
ARG gid=1000

RUN <<EOF
export DEBIAN_FRONTEND=noninteractive
# Disables interactive prompts that may arise during configuration of packages
apt update -y && apt upgrade -y

apt install -y software-properties-common
add-apt-repository -y ppa:graphics-drivers/ppa
# Driver versions on the host and container *must* match.
# Otherwise:
# X Error of failed request:  BadValue (integer parameter out of range for operation)
# Major opcode of failed request:  152 (GLX)
# Minor opcode of failed request:  24 (X_GLXCreateNewContext)
# So install latest Nvidia drivers to match ArchLinux drivers.

# Neovim in Ubuntu is old. Install latest neovim
# The stable PPA repository has an old version instead of the latest stable, so use unstable
add-apt-repository -y ppa:neovim-ppa/unstable


yes | unminimize
apt-get install -y \
    nvidia-driver-535 \
    python3-pip python3-venv \
    neovim \
    ripgrep \
    tmux \
    zsh \
    zsh-syntax-highlighting zsh-autosuggestions \
    bash-completion \
    iputils-ping \
    python-is-python3 \
    ipython3 \
    iproute2 \
    ros-humble-desktop-full

# iputils2 for `ip addr` etc

update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
EOF


# Alternative RMW for testing
RUN apt install -y ros-humble-rmw-cyclonedds-cpp

# For converting quaternions to euler angles
# Alternatively: ros-humble-tf-transformations
RUN apt install -y python3-transforms3d

# turtlebot3-manipulation dependencies according to 
# https://emanual.robotis.com/docs/en/platform/turtlebot3/manipulation/#software-setup
# Somehow turtlebot3-manipulation packages are not released and must be installed manually
# using the instructions above
RUN <<EOF
apt install -y \
    ros-humble-dynamixel-sdk \
    ros-humble-ros2-control \
    ros-humble-ros2-controllers \
    ros-humble-gripper-controllers \
    ros-humble-moveit \
    ros-humble-moveit-servo \
    ros-humble-cartographer-ros \
    ros-humble-navigation2 \
    ros-humble-gazebo-ros2-control \
    ros-humble-gazebo-plugins \
    ros-humble-apriltag-ros \
    ros-humble-ign-ros2-control-demos \
    ros-humble-gazebo-ros2-control-demos \
    'ros-humble-turtlebot3-manipulation-*' \
    'ros-humble-turtlebot3-*'
EOF


# FIXME remove this after if turtlebot3-manipulation works
RUN apt install -y ros-humble-turtlebot3
# LDS-02 fix
RUN <<EOF
sed -i \
    -e 's|<min>0.120000</min>|<min>0.160</min>|' \
    -e 's|<max>3.5</max>|<max>8</max>|' \
    /opt/ros/humble/share/turtlebot3_gazebo/models/turtlebot3_waffle_pi/model.sdf
EOF

# For building the mazes
RUN apt install -y python3-mako

RUN <<EOF
curl -L https://sourceforge.net/projects/virtualgl/files/3.1/virtualgl_3.1_amd64.deb/download -o vgl.deb
apt install -y ./vgl.deb
rm vgl.deb
EOF

# Codium
# Warning: must be started like this: `codium --no-sandbox`
RUN <<EOF
curl https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg
echo 'deb [ signed-by=/etc/apt/trusted.gpg.d/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
apt update && apt install -y codium
EOF

VOLUME /home/u
RUN <<EOF
useradd -m u -G sudo
usermod --uid $uid u
usermod --gid $gid --shell /bin/zsh u
sed -i 's/bash/zsh/g' /ros_entrypoint.sh
EOF
COPY <<EOF /etc/tmux.conf
set -g mouse on
#set -g history-limit 10000
EOF
COPY <<EOF /etc/zsh/zshrc
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
EOF
RUN echo '%sudo ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/sudo-without-pass
USER u
WORKDIR /home/u
ENV DISPLAY=:0
CMD /ros_entrypoint.sh
# Warning
# If you use zsh, only use `*/local_setup.sh`, otherwise autocompletion gets
# overwritten. See https://github.com/ros2/ros2cli/pull/750
# TODO
# sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
# substring search? just add the one bundled in ohmyzsh
# 
# automatically load the turtlebot3 workspace on entry. Then following tmux windows don't have to source
# pay attention to local setup:
# . ./ws/install/local_setup.zsh
# Add to /etc/zsh/zshrc?
# eval "$(register-python-argcomplete3 ros2)"
# eval "$(register-python-argcomplete3 colcon)"
# eval "$(register-python-argcomplete3 rosidl)"
